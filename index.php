<div id=title>
Python Youtube API Web Interface
<br>
<br>
random channel ID: UCRA1O1myxsOJmL5BlG1Qavw
</div>
<br>
<br>
<span class=fake_console>
&gt /usr/local/bin/python3.10 &nbsp python_youtube_api.py &nbsp 
</span>
<input id=user_sid placeholder="User SID">
</input>
&nbsp
<input id=channel_id placeholder="Youtube Channel ID">
</input>
<br>
<br>
<br>
<br>
<center>
<div id=exec>Execute</div>
<br>
<br>
</center>
<pre>
<code class=fake_console id=console_output></code>
</pre>
<style>
.fake_console{
	color:lime;
	max-width:900px;
	margin:0 auto;
}
#exec{
	display:flex;
	flex-direction:column;
	padding:2vh;
	color:black;
	border-radius:.5vh;
	justify-content:center;
	width:min-content;
	text-align:center;
	background:grey;
}
:root{
	font-size:2vh;
	background:black;
	color:white;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script><script>
document.getElementById('exec').onclick=function(){
	var channel_id=document.getElementById('channel_id').value;
	var user_sid=document.getElementById('user_sid').value;

	$.ajax({
		method:"POST",
		url:"exec.php",
		dataType:'json',
		data:{
			user_sid:user_sid,
			channel_id:channel_id
		},
		success:function(response){
			var about=JSON.stringify(JSON.parse(response[0]), null, 4);
			document.getElementById('console_output').innerHTML="results/"+user_sid+"/"+channel_id+"/about.json<br><br>"+about+"<br><br>";		
			var videos=JSON.stringify(JSON.parse(response[1]), null, 4);
			document.getElementById('console_output').innerHTML+="results/"+user_sid+"/"+channel_id+"/videos.json<br><br>"+videos;		
		}
	});
}
</script>
