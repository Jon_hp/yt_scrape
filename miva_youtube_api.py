###################################################################
# www.vidwatchr.com
# google_api_client.py version 1.1
# Repo: https://gitlab.com/Jon_hp/yt_scrape.git
# Python Version: 3.10
# Developer: Jean-Michel Harvey Perron https://gitlab.com/Jon_hp
################################################################

import os
import sys
import json
from googleapiclient.discovery import build

user_sid 			=	sys.argv[1]
channel_id			=	sys.argv[2]
outputs_dir_name	= 	"results/test1234"
videos_file_name 	= 	outputs_dir_name + "videos.json"
channel_file_name 	=	outputs_dir_name + "channel.json"
max_results 		=	10

vidwatchr_key 		= 	"AIzaSyB1CnHElCiFBA3Bzpas_1GWFbFPnXD1Huo"
hawkins_key 		=	"AIzaSyCOOamti4jNmQzX8QyF0VeaGBU3MdGAFPg"

# Try Vidwatchr API key:
try:
	API				= 	build('youtube','v3',developerKey=vidwatchr_key)
	snippet			= 	API.search().list(part="snippet",type="channel",q=channel_id).execute()
# Other API key
except:
	API				= 	build('youtube','v3',developerKey=hawkins_key)
	snippet			= 	API.search().list(part="snippet",type="channel",q=channel_id).execute()

# (rest of) Channel informations:
contentDetails 			=	API.channels().list(part="contentDetails",id=channel_id).execute()
statistics 				=	API.channels().list(part="statistics",id=channel_id).execute()
# Channel Uploads "playlist" id:
playlist_id				= 	contentDetails['items'][0]['contentDetails']['relatedPlaylists']['uploads']
# Videos informations:
playlist_items			=	API.playlistItems().list(part="snippet",playlistId=playlist_id,maxResults=10).execute()

channel={}
# Extract "About" selected Keys
channel['title']		=	snippet['items'][0]['snippet']['title']
channel['description']	=	snippet['items'][0]['snippet']['description']
channel['thumbnail']	=	snippet['items'][0]['snippet']['thumbnails']['default']['url']
channel['subscribers']	=	statistics['items'][0]['statistics']['subscriberCount']
channel['joined']		=	snippet['items'][0]['snippet']['publishedAt']
channel['views']		=	statistics['items'][0]['statistics']['viewCount']
channel['url'] 			=	"https://www.youtube.com/" + channel_id

videos=['']*max_results
# Extract Videos Data
for i in range(0,len(playlist_items['items'])):
	videos[i]={}
	videos[i]['title']=playlist_items['items'][i]['snippet']['title']
	video_id = playlist_items['items'][i]['snippet']['resourceId']['videoId']
	videos[i]['url']="https://www.youtube.com" + video_id
	videos[i]['thumbnail']=playlist_items['items'][i]['snippet']['thumbnails']['default']['url']
	infos = API.videos().list(part="statistics",id=video_id).execute()
	videos[i]['views'] = infos['items'][0]['statistics']['viewCount']

if not os.path.exists(outputs_dir_name):
    os.makedirs(outputs_dir_name)

channel_file = open(channel_file_name,'w')
channel_file.write(json.dumps(channel,indent=4))
channel_file.close()

videos_file = open(videos_file_name,'w')
videos_file.write(json.dumps(videos,indent=4))
videos_file.close()
