import xmltodict
import requests
import json
import sys
channel_id=sys.argv[1]
rss="https://youtube.com/feeds/videos.xml?channel_id={}".format(channel_id)
response=requests.get(rss)
data_dict=xmltodict.parse(response.text)
json_data=json.dumps(data_dict,indent=4)
all_jobs=json.loads(json_data)
print(json_data.replace('@rel','rel').replace('@href','href').replace('media:','media_').replace('yt:','yt_').replace('@xml','xml').replace('@url','url').replace('@type','type').replace('@width','width').replace('@height','height').replace('@count','count').replace('@average','average').replace('@views','views').replace('@max','max').replace('@min','min').replace('xmlns:','xmlns_'))

