###################################################################
# www.vidwatchr.com                        
# google_api_client.py version 1.5        
# Repo: https://gitlab.com/Jon_hp/yt_scrape.git  
# Python Version: 3.10                              
# Developer: Jean-Michel Harvey Perron https://gitlab.com/Jon_hp
################################################################

from googleapiclient.discovery import build
import json
import sys
import os

max_results 	=	10
vidwatchr_key 	= 	"AIzaSyB1CnHElCiFBA3Bzpas_1GWFbFPnXD1Huo"
other_key		=	""
user_sid 		=	sys.argv[1]
channel_id		=	sys.argv[2]

# Try Vidwatchr API key:
try:
	API			= 	build('youtube','v3',developerKey=vidwatchr_key)
	snippet		= 	API.search().list(part="snippet",type="channel",q=channel_id).execute()
# Other key
except:
	API			= 	build('youtube','v3',developerKey=other_key)
	snippet		= 	API.search().list(part="snippet",type="channel",q=channel_id).execute()
	
# (rest of) Channel informations:
contentDetails 	=	API.channels().list(part="contentDetails",id=channel_id).execute()
statistics 		=	API.channels().list(part="statistics",id=channel_id).execute()
# Channel Uploads "playlist" id:
playlist_id		= 	contentDetails['items'][0]['contentDetails']['relatedPlaylists']['uploads']
# Videos informations:
playlist_items	=	API.playlistItems().list(part="snippet",playlistId=playlist_id,maxResults=10).execute()

channel={}
# Extract "About" selected Keys
channel['title']			=	snippet['items'][0]['snippet']['title']
if snippet['items'][0]['snippet']['description'] == '':
	channel['description']	= 	':x:'
else:
	channel['description']	=	snippet['items'][0]['snippet']['description']
channel['thumbnail']		=	snippet['items'][0]['snippet']['thumbnails']['default']['url']
if "subscriberCount" in statistics:
	channel['subscribers']	=	statistics['items'][0]['statistics']['subscriberCount']
else:
	channel['subscribers']	=	':x:'
channel['joined']			=	snippet['items'][0]['snippet']['publishedAt']
channel['views']			=	statistics['items'][0]['statistics']['viewCount']
channel['url'] 				=	"https://www.youtube.com/channel/" + channel_id

videos = [''] * max_results
# Extract Videos Data
for i in range(0,len(playlist_items['items'])):
	video_id 				=	playlist_items['items'][i]['snippet']['resourceId']['videoId'] 
	new_query	 			= 	API.videos().list(part="statistics",id=video_id).execute()
	videos[i]				=	{}

	videos[i]['views'] 		= 	new_query['items'][0]['statistics']['viewCount']
	videos[i]['url']		=	"https://www.youtube.com/watch?v=" + video_id
	videos[i]['title']		=	playlist_items['items'][i]['snippet']['title']
	videos[i]['thumbnail']	=	playlist_items['items'][i]['snippet']['thumbnails']['default']['url']

goData_column='A'
for video_data in "title", "url", "thumbnail", "views":
	goData_row=1
	for video in videos:
		print("<MvASSIGN NAME=\"goData{}{}\"  VALUE=\"{{ '{}' }}\">".format(goData_row,goData_column,video[video_data]))
		goData_row+=1
	goData_column=chr(ord(goData_column)+1)
for channel_data in "title", "description", "thumbnail", "subscribers", "joined", "views", "url":
	print("<MvASSIGN NAME=\"goData{}\"  VALUE=\"{{ '{}' }}\">".format(goData_row,channel[channel_data]))
	goData_row+=1
